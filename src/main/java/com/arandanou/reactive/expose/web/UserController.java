package com.arandanou.reactive.expose.web;

import com.arandanou.reactive.business.UserService;
import com.arandanou.reactive.model.api.UserRq;
import com.arandanou.reactive.model.api.UserRs;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RequestMapping("/user")
@RequiredArgsConstructor
@RestController
public class UserController {

    private final UserService userService;

    @PostMapping("/save")
    public Mono<UserRs> register(@RequestBody UserRq userRq) {
        return userService.save(userRq);
    }

}
