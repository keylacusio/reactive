package com.arandanou.reactive.model.api;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class UserRs {

    private String id;
    private String email;
    private String names;
    private String surnames;
    private String nationality;
    private String documentNumber;
    private String documentType;
    private String gender;
    private Integer age;

}
