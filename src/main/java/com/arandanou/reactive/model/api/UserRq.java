package com.arandanou.reactive.model.api;

import lombok.Data;

@Data
public class UserRq {

    private String id;
    private String email;
    private String names;
    private String surnames;
    private String nationality;
    private String documentNumber;
    private String documentType;
    private String gender;
    private Integer age;

}
