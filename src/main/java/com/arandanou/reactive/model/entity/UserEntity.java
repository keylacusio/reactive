package com.arandanou.reactive.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;

@Document(collection = "userList")
@NoArgsConstructor
@Data
public class UserEntity {

    @Id
    @NotEmpty
    private String id;
    private String email;
    private String names;
    private String surnames;
    private String nationality;
    private String documentNumber;
    private String documentType;
    private String gender;
    private Integer age;
}


