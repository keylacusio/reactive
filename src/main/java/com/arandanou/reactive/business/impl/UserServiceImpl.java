package com.arandanou.reactive.business.impl;

import com.arandanou.reactive.business.UserService;
import com.arandanou.reactive.model.api.UserRq;
import com.arandanou.reactive.model.api.UserRs;
import com.arandanou.reactive.model.entity.UserEntity;
import com.arandanou.reactive.repository.UserMapper;
import com.arandanou.reactive.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public Mono<UserRs> save(UserRq userRq) {
        Mono<UserEntity> save = userRepository.save(UserMapper.INSTANCE.toEntity(userRq));
        return UserMapper.INSTANCE.toRs(save);
    }

}
