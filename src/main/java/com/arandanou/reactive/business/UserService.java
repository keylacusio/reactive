package com.arandanou.reactive.business;

import com.arandanou.reactive.model.api.UserRq;
import com.arandanou.reactive.model.api.UserRs;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

public interface UserService {

    Mono<UserRs> save(UserRq userRq);

}
