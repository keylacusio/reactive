package com.arandanou.reactive.repository;

import com.arandanou.reactive.model.api.UserRq;
import com.arandanou.reactive.model.api.UserRs;
import com.arandanou.reactive.model.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import reactor.core.publisher.Mono;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    default UserEntity toEntity(UserRq userRq) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userRq.getId());
        userEntity.setEmail(userRq.getEmail());
        userEntity.setNames(userRq.getNames());
        userEntity.setSurnames(userRq.getSurnames());
        userEntity.setNationality(userRq.getNationality());
        userEntity.setDocumentNumber(userRq.getDocumentNumber());
        userEntity.setDocumentType(userRq.getDocumentType());
        userEntity.setGender(userRq.getGender());
        userEntity.setAge(userRq.getAge());
        return userEntity;

    }

    default Mono<UserRs> toRs(Mono<UserEntity> userEntity) {
        return userEntity.map(e -> UserRs.builder()
                .id(e.getId())
                .email(e.getEmail())
                .names(e.getNames())
                .surnames(e.getSurnames())
                .nationality(e.getNationality())
                .documentNumber(e.getDocumentNumber())
                .documentType(e.getDocumentType())
                .gender(e.getGender())
                .age(e.getAge())
                .build());
    }

}
